import React, { Component } from 'react';
import Header from './Header/Header';
import Featured from './Featured/index';
import LiveFeed from './LiveUpdates/liveIndex';
import Table from './Tablefeed/Tablefeed';
import Graph from './Graph/Graph';
import CountryTable from './CountriesAffected/CountriesAffected';
import axios from 'axios';

class appcomponent extends Component {
  constructor() {
    super();

    this.state = {
      data: []
    };
  }

  componentDidMount() {
    axios
      .get(`${process.env.REACT_APP_API_BASE_URL}/api/virus-data/`)
      .then(res => {
        this.setState({ data: res.data });

        console.log(res.data);
        console.log('virus data loaded');
      })
      .catch(err => {
        console.log(err);
        console.log('virus data not loaded');
      });
  }

  render() {
    return this.state.data.length === 0 ? (
      <div />
    ) : (
        <div>
          <Header />
          <Featured mapData={this.state.data} />
          <LiveFeed data={this.state.data} />
          <Table data={this.state.data} />
          <CountryTable data={this.state.data} />
          <Graph />
          
          <footer class="page-footer font-small bck_black" >
            <div class="footer-copyright text-center py-3 text-white">© 2020 Copyright:
              <a href={`${process.env.REACT_APP_API_BASE_URL}/BossLabs`} target="_blank"> BossLabs.com</a>
            </div>
          </footer>

        </div>
      );
  }
}

export default appcomponent;
