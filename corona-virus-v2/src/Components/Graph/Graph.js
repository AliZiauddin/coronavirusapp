import React, { Component } from 'react';
import axios from 'axios';

import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  Tooltip,
  CartesianGrid,
} from 'recharts';

class Graphs extends Component {
  constructor() {
    super();
    this.state = {
      chartData: []
    };

    this.fontSize = '80%';
  }

  componentDidMount() {

    axios
      .get(`${process.env.REACT_APP_API_BASE_URL}/api/virus-data/time-series`)
      .then(res => {
        this.setState({ chartData: res.data });

        console.log(res.data);
        console.log('time series loaded');
      })
      .catch(err => {
        console.log(err);
        console.log('time series not loaded');
      });
  }



  render() {
    return (
      <div className="graph-group">
        <div className="row">
          {this.state.chartData.confirmed_cases === undefined ? <div /> : (
            <div className="col-xs-4 m-2 ml-5 graph-card">
              <h4 style={{margin: 'auto', textAlign:'center', color:'#3c3c3c'}}>Confirmed Cases</h4>
              <LineChart
                width={window.innerWidth<500?320:480}
                height={window.innerWidth<500?240:360}
                data={this.state.chartData.confirmed_cases}
                margin={{ top: 5, right: 10, left: 0, bottom: 5 }}
              >
                <XAxis dataKey="date" tick={{fontSize: this.fontSize}}/>
                <YAxis type="number" domain={[0, dataMax=>(Math.ceil(dataMax/1000)*1000)]} tick={{fontSize: this.fontSize}}/>
                <Tooltip />
                <CartesianGrid stroke="#f5f5f5" />
                <Line
                  type="monotone"
                  dataKey="count"
                  stroke="#ff7300"
                  yAxisId={0}
                  dot={null}
                />
                {/* <Line type="monotone" dataKey="pv" stroke="#387908" yAxisId={1} /> */}
              </LineChart>

            
            </div>)
          }
          {this.state.chartData.deaths === undefined ? <div /> : (
            <div className="col-xs-4 m-2 graph-card">
              <h4 style={{margin: 'auto', textAlign:'center', color:'#3c3c3c'}}>Deaths</h4>
              <LineChart
                width={window.innerWidth<500?320:480}
                height={window.innerWidth<500?240:360}
                data={this.state.chartData.deaths}
                margin={{ top: 5, right: 10, left: 0, bottom: 5 }}
              >
                <XAxis dataKey="date" tick={{fontSize: this.fontSize}}/>
                <YAxis type="number" domain={[0, dataMax=>(Math.ceil(dataMax/1000)*1000)]} tick={{fontSize: this.fontSize}}/>
                <Tooltip />
                <CartesianGrid stroke="#f5f5f5" />
                <Line
                  type="monotone"
                  dataKey="count"
                  stroke="#ff7300"
                  yAxisId={0}
                  dot={null}
                />
                {/* <Line type="monotone" dataKey="pv" stroke="#387908" yAxisId={1} /> */}
              </LineChart>
              
            </div>)
          }
          {this.state.chartData.recovered === undefined ? <div /> : (
            <div className="col-xs-4 m-2 mr-5 graph-card" >
              <h4 style={{margin: 'auto', textAlign:'center', color:'#3c3c3c'}}>Recovered</h4>
              <LineChart
                width={window.innerWidth<500?320:480}
                height={window.innerWidth<500?240:360}
                data={this.state.chartData.recovered}
                margin={{ top: 5, right: 20, left: 10, bottom: 5 }}
              >
                <XAxis dataKey="date" tick={{fontSize: this.fontSize}}/>
                <YAxis type="number" domain={[0, dataMax=>(Math.ceil(dataMax/1000)*1000)]} tick={{fontSize: this.fontSize}}/>
                <Tooltip />
                <CartesianGrid stroke="#f5f5f5" />
                <Line
                  type="monotone"
                  dataKey="count"
                  stroke="#ff7300"
                  yAxisId={0}
                  dot={null}
                />
                {/* <Line type="monotone" dataKey="pv" stroke="#387908" yAxisId={1} /> */}
              </LineChart>
              
            </div>)}
        </div>
      </div>
    );
  }
}

export default Graphs;
