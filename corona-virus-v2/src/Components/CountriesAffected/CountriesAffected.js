import React, { Component } from 'react'
import Table from 'react-bootstrap/Table'


export default class CountriesAffected extends Component {

  constructor(props) {
    super(props)

    const sortedData = this.props.data;

    sortedData.sort((a,b)=>{
      if(a.TotalCases<b.TotalCases) 
        return 1 
      else if (a.TotalCases>b.TotalCases)
        return -1
      else if (a.TotalDeaths<b.TotalDeaths) 
        return 1
      else
        return -1

    })

    this.state = {
      data: sortedData
    }
  }



  render() {
    return (
      <div className="CountriesAffected">
        <div classsName="tableHead">
          <span>
            <h1 id="Countryhead">Countries / Regions Affected</h1>
          </span>
        </div>

        <div className="table">
          <Table striped bordered hover variant="dark" style={{overflowX:'scroll'}}>
            <thead>
              <tr>
                <th className="tableheaderone">
                  Total:{this.state.data.length - 1}
                </th>
                <th className="tableheaderone">
                  Infections&emsp;&emsp;&emsp;
                  </th>
                  <th className="tableheaderone">
                  Deaths&emsp;&emsp;&emsp;
                  </th>
                  <th className="tableheaderone">
                  Recovered&emsp;&emsp;&emsp;
                  </th>
                  <th className="tableheaderone">
                  Death Rate&emsp;&emsp;&emsp;
                  </th>
                  <th className="tableheaderone">
                  Recovery Rate&emsp;&emsp;&emsp;
                  </th>

              </tr>
            </thead>
            <tbody>
              {this.state.data.map((country) => (
                <tr>
                  <td>
                    {country.Country === 'World'?`${country.Country}`:`${country.Country} (${country.Code})`}
                  </td>
                  <td>
                    {country.TotalCases}
                  </td>
                  <td>
                    {country.TotalDeaths}
                  </td>
                  <td>
                    {country.TotalRecovered}
                  </td>
                  <td>
                    {`${country.TotalCases == 0?(0).toFixed(2):((country.TotalDeaths/country.TotalCases)*100).toFixed(2)} %`}
                  </td>
                  <td>
                    {`${country.TotalCases == 0?(0).toFixed(2):((country.TotalRecovered/country.TotalCases)*100).toFixed(2)} %`}
                  </td>

                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </div>
    )
  }
}
