import React from 'react';
import BackgroundMap from './Map';
import CountDown from './CountDown';

class index extends React.Component {

  constructor(props){
    super(props);
    this.state={
      data:this.props.mapData,
      hoveredCountryData: this.props.mapData[this.props.mapData.length-1],
      counterHeight: 0
      
    }
    this.worldTotal = this.props.mapData[this.props.mapData.length-1]
  }

  handleCountryHover = (countryData)=>{
    if(countryData){
      this.setState({
        hoveredCountryData: countryData
      })
    }
    else{
      this.setState({
        hoveredCountryData: this.worldTotal
      })
    }
    // console.log(countryData)
  }

  handleCountryLeave = ()=>{
    this.setState({
      hoveredCountryData: this.worldTotal
    })
    // console.log('Country Left')
  }

  handleCounterHeight = (height) => {
    this.setState({
      counterHeight: height
    })
    
  }
  

  render() {

    console.log(this.state.hoveredCountryData)
    return (
      <div style={{ position: 'relative' }} 
      style={{
        height: `${window.innerHeight}px`
      }}>


        <BackgroundMap 
        mapData={this.state.data}
        onCountryHover={this.handleCountryHover}
        onCountryLeave={this.handleCountryLeave}
        counterHeight={this.state.counterHeight}/>


        <CountDown total={this.state.hoveredCountryData} 
        onHeightMeasured = {this.handleCounterHeight}/>
      </div>
    );

  }
};

export default index;