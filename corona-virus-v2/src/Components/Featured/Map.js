  import React, { Component } from 'react';
import {
  ComposableMap,
  Geographies,
  Geography,
  ZoomableGroup
} from 'react-simple-maps';
// import back from '../../Components/IMAGE3.jpg';

// const geoUrl =
//   'https://raw.githubusercontent.com/zcreativelabs/react-simple-maps/master/topojson-maps/world-110m.json';
const geoUrl = `${process.env.REACT_APP_API_BASE_URL}/api/mapTopoData`;
const geoUrlChina =
  'https://raw.githubusercontent.com/deldersveld/topojson/master/countries/china/china-provinces.json';

const colors = ['#F68656','#EC7D58', '#E1674E', '#CF443D', '#C53134', '#74181A'];
const hoverColors = ['#EA5A3E','#F78762', '#EA7057‬', '#D84D46', '#CE3A3D', '#7D2123'];


class Map extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mapData: this.props.mapData,
     
    };

  }

  // shouldComponentUpdate(){
  //   return false
  // }


  handleMoveMap = (geo) =>{
    // console.log(geo.data);

    this.props.onCountryHover(geo.data);
  }



  handleLeaveMap = () => {
    this.props.onCountryLeave();
  };

  hoverColorWRTData = d => {
    const countryCode = d.properties.ISO_A2;

    const { mapData } = this.state;

    let color = '#E7E7E7';
    mapData.forEach(item => {
      if (item.Code === countryCode) {
        d.data = item;

        if (item.ActiveCases > 100000) color = hoverColors[5];
        else if (item.ActiveCases > 10000) color = hoverColors[4];
        else if (item.ActiveCases > 1000) color = hoverColors[3];
        else if (item.ActiveCases > 100) color = hoverColors[2];
        else if (item.ActiveCases > 10) color = hoverColors[1];
        else if (item.ActiveCases > 1) color = hoverColors[0];
      }
    });

    return color;
  };

  fillColorWRTData = d => {
    const countryCode = d.properties.ISO_A2;

    const { mapData } = this.state;

    let color = '#DDDDDD';
    mapData.forEach(item => {
      if (item.Code === countryCode) {
        if (item.ActiveCases > 100000) color = colors[5];
        else if (item.ActiveCases > 10000) color = colors[4];
        else if (item.ActiveCases > 1000) color = colors[3];
        else if (item.ActiveCases > 100) color = colors[2];
        else if (item.ActiveCases > 10) color = colors[1];
        else if (item.ActiveCases > 1) color = colors[0];
      }
    });

    return color;
  };
  render() {
    console.log("CounterHeight: " + window.innerHeight/parseFloat(this.props.counterHeight))
    return (
      <div
        className="carrousel_wrapper"
        style={{
          height: this.props.counterHeight === 0?`${window.innerHeight}px`: `${window.innerHeight - this.props.counterHeight +5}px`,
          overflow: 'hidden'
        }}
      >
        <div>
          <div
            className="carrousel_image"
            style={{
              background: `#EEEEEE`,
              height: `${window.innerHeight}px`
            }}
            onMouseMove={this.handleMoveParent}
          >
            {this.toolTip}


            <ComposableMap style={{ width: '100%', height: '100%' }}>
              <ZoomableGroup zoom={window.innerWidth<500?1.1:0.7} disablePanning={true} 
              center = {window.innerWidth<500 && this.props.counterHeight?[0,window.innerHeight/parseFloat(this.props.counterHeight)*-25]:[0,0]}>
                <Geographies geography={geoUrl}>
                  {(geographies, projection) =>
                    geographies.map((geography, i) => (
                      <Geography
                        key={i}
                        cacheId={geography.properties.ISO_A3 + i}
                        geography={geography}
                        projection={projection}
                        onMouseMove={this.handleMoveMap}
                        onMouseLeave={this.handleLeaveMap}
                        style={{
                          default: {
                            fill: this.fillColorWRTData(geography),
                            stroke: '#242424',
                            strokeWidth: 0.25,
                            outline: 'none',
                            transition: 'all 250ms'
                          },
                          hover: {
                            fill: this.hoverColorWRTData(geography),
                            stroke: '#000000',
                            strokeWidth: 0.75,
                            outline: 'none',
                            transition: 'all 250ms'
                          },
                          pressed: {
                            fill: this.hoverColorWRTData(geography),
                            stroke: '#000000',
                            strokeWidth: 0.75,
                            outline: 'none',
                            transition: 'all 250ms'
                          }
                        }}
                      />
                    ))
                  }
                </Geographies>
              </ZoomableGroup>
            </ComposableMap>
          </div>
        </div>
      </div>
    );
  }
}

export default Map;
