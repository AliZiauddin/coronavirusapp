import React, { Component } from 'react';
import {Fade} from 'react-bootstrap';
class CountDown extends Component {

  constructor(props) {
    super(props)

    this.state = {

      total_data: this.props.total

    }

  }

  // componentWillUpdate(){
  //   return true;
  // }

  // componentWillReceiveProps(){
  //   return this.state.total_data.Code !== this.props.total;
  // }


  componentDidMount(){
    if(this.divElement)
      this.props.onHeightMeasured(this.divElement.clientHeight)
  }
  
  render() {
    // console.log("Totals rerendered")
    // console.log(this.state.total_data)
    if(this.props.total !== undefined){
    if(this.props.total.Country === "World")
    return (

      // <Fade in={true} timeout={3000}>

        <div className="main_box">
          <div className="top_box">
            World
          </div>

          <div className="bottom_box" 
          ref={ (divElement) => { this.divElement = divElement } }>
              <div className="bottom_inner_box">
                  <div className="stats_value">
                    {this.props.total.TotalCases}
                  </div>
                  <div className="stats_name">
                    Total Infected
                  </div>
              </div>
              <div className="bottom_inner_box">
                  <div className="stats_value">
                    {this.props.total.TotalDeaths}
                  </div>
                  <div className="stats_name">
                    Total Deaths
                  </div>
            </div>
            <div className="bottom_inner_box">
                  <div className="stats_value">
                    {this.props.total.TotalRecovered}
                  </div>
                  <div className="stats_name">
                    Total Recovered
                  </div>
            </div>
            <div className="bottom_inner_box">
                  <div className="stats_value">
                    {this.props.total.ActiveCases}
                  </div>
                  <div className="stats_name">
                    Active Cases
                  </div>
            </div>
              <div className="bottom_inner_box">
                  <div className="stats_value">
                    {this.props.total.NewCases}
                  </div>
                  <div className="stats_name">
                    New Cases
                  </div>
              </div>
            
            
            <div className="bottom_inner_box">
                  <div className="stats_value">
                    {this.props.total.NewDeaths}
                  </div>
                  <div className="stats_name">
                    New Deaths
                  </div>
            </div>

            
            
            <div className="bottom_inner_box">
                  <div className="stats_value">
                    {this.props.total.TotCases_1M_Pop}
                  </div>
                  <div className="stats_name">
                    Total Cases 1M Pop
                  </div>
            </div>
            <div className="bottom_inner_box">
                  <div className="stats_value">
                    {this.props.total['Deaths/1M pop']}
                  </div>
                  <div className="stats_name">
                    Total Deaths 1M Pop
                  </div>
            </div>

            {/* <div className="bottom_inner_box">
                  <div className="stats_value">
                    {this.props.total.TotalCountries}
                  </div>
                  <div className="stats_name">
                    Countries
                  </div>
            </div> */}
            
            

          </div>
        </div>



      // </Fade>

    );
    else
    return (

      // <Fade in={true} timeout={3000}>

        <div className="main_box">
          <div className="top_box">
            {this.props.total.Country}
          </div>
          
          <div className="bottom_box">
          <div className="bottom_inner_box">
                  <div className="stats_value">
                    {this.props.total.TotalCases}
                  </div>
                  <div className="stats_name">
                    Total Infected
                  </div>
              </div>
              <div className="bottom_inner_box">
                  <div className="stats_value">
                    {this.props.total.TotalDeaths}
                  </div>
                  <div className="stats_name">
                    Total Deaths
                  </div>
            </div>
            <div className="bottom_inner_box">
                  <div className="stats_value">
                    {this.props.total.TotalRecovered}
                  </div>
                  <div className="stats_name">
                    Total Recovered
                  </div>
            </div>
            <div className="bottom_inner_box">
                  <div className="stats_value">
                    {this.props.total.ActiveCases}
                  </div>
                  <div className="stats_name">
                    Active Cases
                  </div>
            </div>
              <div className="bottom_inner_box">
                  <div className="stats_value">
                    {this.props.total.NewCases}
                  </div>
                  <div className="stats_name">
                    New Cases
                  </div>
              </div>
            
            <div className="bottom_inner_box">
                  <div className="stats_value">
                    {this.props.total.NewDeaths}
                  </div>
                  <div className="stats_name">
                    New Deaths
                  </div>
            </div>
            <div className="bottom_inner_box">
                  <div className="stats_value">
                    {this.props.total.TotCases_1M_Pop}
                  </div>
                  <div className="stats_name">
                    Total Cases 1M Pop
                  </div>
            </div>
            <div className="bottom_inner_box">
                  <div className="stats_value">
                    {this.props.total['Deaths/1M pop']}
                  </div>
                  <div className="stats_name">
                    Total Deaths 1M Pop
                  </div>
            </div>

          </div>
        </div>



      // </Fade>

    );
  }
  else
  return <div/>
  }

}

export default CountDown;