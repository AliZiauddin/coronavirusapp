import React, { Component } from 'react';
import Table from 'react-bootstrap/Table';

class Tablefeed extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: this.props.data[this.props.data.length - 1]
    };
  }

  render() {
    return (
      <div className="Tablefeed">
        <span className="Tablefeed-header">
          <h1 className="head-table">Statistics</h1>
          <h3>Source: World Health Organization</h3>
        </span>

        <div className="container" style={{ marginTop: 50 }} >
          <Table striped bordered hover variant="dark"  >
            <tbody>
              <tr>
                <td style={{width:'50%'}}>NUMBER OF INFECTIONS</td>

                <td style={{width:'50%'}}>{this.state.data.TotalCases}</td>
              </tr>
              <tr>
                <td style={{width:'50%'}}>NUMBER OF DEATHS</td>

                <td style={{width:'50%'}}>{this.state.data.TotalDeaths}</td>
              </tr>
              <tr>
                <td style={{width:'50%'}}>DEATH RATE</td>

                <td style={{width:'50%'}}>{this.state.data.TotalRecovered}</td>
              </tr>
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

export default Tablefeed;
