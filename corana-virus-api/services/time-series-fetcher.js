var fs = require('fs');
const axios = require('axios');
const parse = require('csv-parse');

const client = require('../db/mongo')

const { workerData, parentPort } = require('worker_threads');

const constants = require('../db/db-constants')

class TimeSeriesFetcher {

  constructor() {
    this.fileNames = [
      'time_series_19-covid-Confirmed.csv',
      'time_series_19-covid-Deaths.csv',
      'time_series_19-covid-Recovered.csv'
    ];

    this.outputFileNames = [
      constants.TIME_SERIES_CONFIRMED,
      constants.TIME_SERIES_DEATHS,
      constants.TIME_SERIES_RECOVERED
    ];

    this.index = 0;
  }

  calculateTotalAndWriteToFile = (err, records) => {
    if (err) throw err;

    let totals = [];

    for (let i = 4; i < records[0].length; i++) {
      let object = {};
      object['date'] = records[0][i];
      object['count'] = (function() {
        let sum = 0;
        for (let j = 1; j < records.length; j++) {
          sum += parseInt(records[j][i]);
        }
        return sum;
      })();

      totals.push(object);
    }

    // fs.writeFileSync(
    //   path.join(
    //     __dirname,
    //     '../',
    //     'virus-data',
    //     this.outputFileNames[this.index]
    //   ),
    //   JSON.stringify(totals)
    // );


    client.connect().then(()=>{


      const myQuery = {name: this.outputFileNames[this.index]}
      const newValue = { 
        $set:{
          name : this.outputFileNames[this.index],
          data : totals
        }
      }
      
     
      client.db('heroku_hxx9fxjj' || 'corona_db').collection('time_series')
      .update(myQuery, newValue, {upsert:true}, (err, res)=>{

        if (err) throw err; 


        this.index++;
        this.getCSVdata();
      })

    }).catch((err)=>{
      console.log(err);
    })


    
 

    
  };

  getCSVdata = () => {
    if (this.index < this.fileNames.length) {
      axios
        .get(
          `https://api.github.com/repos/CSSEGISandData/COVID-19/contents/csse_covid_19_data/csse_covid_19_time_series/${
            this.fileNames[this.index]
          }`,
          {
            headers: {
              Authorization:
                '14cb3e7809d52ded11f34ec6176cc4cd801be88f87e8fe2c43696259b7c3ab7c'
            }
          }
        )
        .then(response => {
          // handle success

          const jsonBody = response.data.content;
          // console.log("[time-series-fetcher.js] " ,jsonBody)

          let buff = Buffer.alloc(jsonBody.length, jsonBody, 'base64');

          let Gitcontent = buff.toString('ascii');

          let csvString = Gitcontent.replace(/"[^"]+"/g, function(v) {
            return v.replace(/,/g, '-').replace(' ', '');
          });

          const end = csvString.indexOf(
            'Province/State',
            'Province/State'.length
          );

          csvString = csvString.substring(0, end);

          parse(csvString.trim(), {}, this.calculateTotalAndWriteToFile);
        })
        .catch(function(error) {
          // handle error
          console.log(error);
        })
        .then(function() {
          // always executed
        });
    } else {
      parentPort.postMessage('Time Series Data Fetched and Loaded');
    }
  };
}

new TimeSeriesFetcher().getCSVdata();
