exports.parseCsvString = csvString => {
  var dataPoints = [];
  var sumAffectedCountries = 0;
  var sumConfirmed = 0;
  var sumDeaths = 0;
  var sumRecovered = 0;
  var data = [];
  var jsonData;
  var lineCount = 0;

  //csvString = csvString.replace('', 'Null')
 

  var r = csvString.replace(/"[^"]+"/g, function(v) {
    return v.replace(/,/g, '-').replace(' ', '');
  });

  //const end = r.indexOf('Province/State', 'Province/State'.length);
  const end = r.indexOf('FIPS', 'FIPS'.length);
  r = r.substring(0, end);

  dataPoints = r
    .replace(/\n/g, ',')
    .replace(/"/g, '')
    .split(',');


  var index = 12;
  
  //FIPS Admin2 Province/State,Country/Region,Last Update,Lat, Long, Confirmed,Deaths,Recovered,Active,CombinedKey
  // 0     1          2            3              4         5   6        7       8       9
  while (index+5 < dataPoints.length && dataPoints[index] != 'FIPS') {
    sumConfirmed += parseInt(dataPoints[index + 7]);
    sumDeaths += parseInt(dataPoints[index + 8]);
    sumRecovered += parseInt(dataPoints[index + 9]);
    sumAffectedCountries += 1;
    
    let newEntry = {
      place: dataPoints[index + 3],
      province: dataPoints[index+2],
      confirmed_cases: parseInt(dataPoints[index + 7]),
      deaths: parseInt(dataPoints[index + 8]),
      recovered: parseInt(dataPoints[index + 9])
    };

    let flag = false;
    for (let i = 0; i < data.length; i++) {
      if (data[i].place == newEntry.place) {
        flag = true;
        data[i].confirmed_cases += newEntry.confirmed_cases;
        data[i].deaths += newEntry.deaths;
        data[i].recovered += newEntry.recovered;

        sumAffectedCountries -= 1;


        break;
      }
    }

    if (!flag) {
      data.push(newEntry);

      if (newEntry.place === 'Others') sumAffectedCountries -= 1;
    }

    index += 12;
  }

  data.push({
    place: 'WORLD',
    confirmed_cases: sumConfirmed,
    deaths: sumDeaths,
    affected_countries: sumAffectedCountries,
    recovered: sumRecovered
  });

  return data;
};
