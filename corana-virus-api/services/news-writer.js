const fs = require('fs');
const path = require('path');
const { workerData, parentPort } = require('worker_threads');
const NewsAPI = require('newsapi');
const newsapi = new NewsAPI('44abcbafefe8454aa0078c14f2c50266');
const client = require("../db/mongo");

const newsData = [];
// You can do any heavy stuff here, in a synchronous way
// without blocking the "main thread"

// fs.writeFileSync(
//   path.join(__dirname, '../', 'data', 'news-data.json'),
//   JSON.stringify(workerData)
// );

newsapi.v2.topHeadlines({
  q: 'corona virus pandemic',
  category: 'health',
  language: 'en',
  pageSize: 100,
}).then(response => {
  // console.log(response);
  const news = response.articles;
  let title = 'abc';
  let i=0;
  // console.log(news[i].title)
  while(i < news.length){
    if(title && title != news[i].title){
      // console.log(i)
      newsData.push(news[i])
    }
    title = news[i].title
    i++;
   
  }

  // const db = client.db("heroku_hxx9fxjj" || "corona_db");
  // const result = await db.collection('news_data').deleteMany({});
  // result = await db.collection('news_data').insertOne({news:newsData});

  // client.close();

  // console.log(newsData)
  client
      .connect()
      .then(() => {
        try {
          try {
            client
              .db("heroku_hxx9fxjj" || "corona_db")
              .collection("news_data")
              .deleteMany({});
          } catch (e) {
            console.log(e);
          }

          client
            .db("heroku_hxx9fxjj" || "corona_db")
            .collection("news_data")
            .insertOne({
              news: newsData
            }).then((res,err) =>{
              if (err) throw err;
              client.close()
              console.log('news added')
            });
            
          // client.close();
        } catch (e) {
          console.log(e);
        }
      })
      .catch(err => {
        console.log(err);
      });
 
});



// parentPort.postMessage({ message: 'News Successfully Fetched.' });

