const covid = require('covid19-api');
var fs = require('fs');
const path = require('path');
const client = require('../db/mongo');
const { workerData, parentPort } = require('worker_threads');

function getCovidData() {
var dataPoints = [];
var data = [];


    covid.getReports().then((res)=>{
        
        let internalData = res[0][0]
        let totalCountries = internalData.table[1].length - 1
        
        data = res[0][0].table[1]
        
        
      for(let i=0;i<totalCountries+1;i++){
          eachObject = data[i]
          
          Object.keys(eachObject).forEach(function(key){ 
            if(key != 'Country'){
              if( eachObject[key] === ""){
                eachObject[key] = 0
              }
              else{
                                                  
                let temp = eachObject[key].replace(/,/g, '')
                eachObject[key] = parseInt(temp) 
              } 
            }
           
          });
          data[i] = eachObject  
        }
       
        // let newEntry = {
        //     TotalCases: internalData.cases ,
        //     NewCases: '' ,
        //     TotalDeaths: internalData.deaths, 
        //     TotalRecovered: internalData.recovered,
        //     ActiveCases : internalData.active_cases[0],
        //     Country: 'World',
        //     TotalCountries: internalData.table[1].length
        // };
        data[totalCountries]['TotalCountries'] = totalCountries
        //newEntry = {TotalCountires: totalCountries}
        //data.push(newEntry);
        // console.log(data)
        loadCountryCodes(data)
        console.log('done')

        


    }).catch(err=>{
        console.log(err)
        
    });


}


////////////--------------------Country Code-------------------////////////


function loadCountryCodes(jsonData) {
    let codesArray = JSON.parse(
      fs.readFileSync(path.join(__dirname, '../', 'country-codes.json'), 'utf8')
    );
  
    jsonData.forEach(object => {
      for (let i = 0; i < codesArray.length; i++) {
        if (object.Country === codesArray[i].name) {
          object['Code'] = codesArray[i].code;
          break;
        }
        else if(object.Country === 'Total:'){
          object['Code'] = '';
          object.Country = "World"
          break;
        }
        else if(object.Country === 'Diamond Princess'){
          object['Code'] = '';
          break;
        }
      }
    });
  
    client.connect().then(()=>{
  
        try{
            try{
            client.db('heroku_hxx9fxjj' || 'corona_db').collection('covid_affected_countries').deleteMany({
                
            })} catch(e){console.log(e)}
            
            client.db('heroku_hxx9fxjj' || 'corona_db').collection('covid_affected_countries').insertOne({
            country_list:jsonData}).then((res,err) =>{
              if (err) throw err;
              client.close()
              console.log('news added')
            });
            // client.close()
            } catch(e){
            console.log(e)
        }
    }).catch((err)=>{
      console.log(err)
    })

  }

  getCovidData();
