const fs = require('fs');
const path = require('path');
const express = require('express')
const router = express.Router()
const client = require('../db/mongo')

const constants = require('../db/db-constants')


// middleware that is specific to this router
router.use((req, res, next) => {
    console.log('Time: ', Date.now())
    next()
})


router.get('/virus-data', (req, res) => {
    // const latestfile = fs.readFileSync(
    //     path.join(__dirname, '../', 'virus-data', 'latestfile.txt'),
    //     'utf8'
    // );
    console.log('in api virus data')
    client.connect().then(()=>{

        client.db('heroku_hxx9fxjj' || 'corona_db').collection('covid_affected_countries').findOne({}, { sort: { _id: -1 }, limit: 1 }, (err, mongoRes)=>{
            console.log('in db mongo to get data')
            if (err) throw err;

            res.setHeader('Content-Type', 'application/json');
            //console.log(mongoRes.country_list)
            res.send(mongoRes.country_list)
            //client.close();
        });

        
    })
    .catch((err)=>{
        console.log(err)
    })

    // const virus_data = fs.readFileSync(
    //     path.join(__dirname, '../', 'virus-data', latestfile.replace('.csv', '.json'))
    // );



    
    
});

router.get('/virus-data/time-series', (req, res) => {
    // const confirmed_cases = fs.readFileSync(
    //     path.join(__dirname, '../', 'virus-data', 'time_series_confirmed.json'),
    //     'utf8'
    // );

    // const deaths = fs.readFileSync(
    //     path.join(__dirname, '../', 'virus-data', 'time_series_deaths.json'),
    //     'utf8'
    // );

    // const recovered = fs.readFileSync(
    //     path.join(__dirname, '../', 'virus-data', 'time_series_recovered.json'),
    //     'utf8'
    // );

    client.connect().then(()=>{

        client.db('heroku_hxx9fxjj' || 'corona_db')
        .collection('time_series')
        .find({}, { $or: [
                {name: constants.TIME_SERIES_CONFIRMED},
                {name: constants.TIME_SERIES_DEATHS}, 
                {name: constants.TIME_SERIES_RECOVERED}
            ] 
        }).toArray( (err, mongoRes)=>{

            if (err) throw err;

            const response = {};
            response[mongoRes[0].name] = mongoRes[0].data;
            response[mongoRes[1].name] = mongoRes[1].data;
            response[mongoRes[2].name] = mongoRes[2].data;
            
        
            res.status(200)
            res.setHeader('Content-Type', 'application/json');
            res.json(response);
        });

        
    })
    .catch((err)=>{
        console.log(err)

        res.status(500)
        res.setHeader('Content-Type', 'application/json');
        res.json({error:'Server error'});
    })

    

});

router.get('/mapTopoData', (req, res) => {
    const world_data = fs.readFileSync(
        path.join(__dirname, '../', 'data', 'world-map.json')
    );

    res.send(world_data);
});

router.get('/news', (req, res) => {

    client.connect().then(()=>{

        client.db('heroku_hxx9fxjj' || 'corona_db').collection('news_data').findOne({}, { sort: { _id: -1 }, limit: 1 }, (err, mongoRes)=>{
            console.log('in db mongo to get data')
            if (err) throw err;

            res.setHeader('Content-Type', 'application/json');
            // console.log(mongoRes.news[0])
            res.send(mongoRes.news)
            //client.close();
        });

        
    })
    .catch((err)=>{
        console.log(err)
    })


    // const world_data = fs.readFileSync(
    //     path.join(__dirname, '../', 'data', 'news-data.json')
    // );
    // res.setHeader('Content-Type', 'application/json');
    // res.send(world_data);
});



module.exports = router;