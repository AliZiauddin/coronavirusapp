const express = require('express');
const app = express();
const port = process.env.PORT || 9000;
const fs = require('fs');
const cors = require('cors');
const cron = require('cron');
const axios = require('axios');
const path = require('path');
const service = require('./services/background-services');
const api = require('./api/api')


app.use(cors());

app.use('/api', api);

app.get('/BossLabs',(req, res)=>{

  res.sendFile('public/BossLabs.html', {root: __dirname })
})

let services = new service.BackgroundServices();

async function runNewsWriter() {
  const result = await services.runNewsWriterService();
  console.log(result);
}

// async function runGitFetcher() {
//   const result = await services.runVirusDataWriterService();
//   console.log(result);
// }

async function runGetCovid() {
    const result = await services.runCovidDataWriterService();
    console.log(result);
  }

async function runTimeSeriesFetcher() {
  const result = await services.runTimeSeriesWriterService();
  console.log(result);
}

const newsJob = cron.job('0 0 * * * *', () => {
  runNewsWriter().catch(err => console.error(err))
  // axios
  //   .get(
  //     'https://newsapi.org/v2/top-headlines?q=corona%20virus&category=health&apiKey=44abcbafefe8454aa0078c14f2c50266'
  //   )
  //   .then(res => {
  //     runNewsWriter(res.data.articles).catch(err => console.error(err));
  //   })
  //   .catch(err => {
  //     console.log(err);
  //   });
});

// const virusDataJob = cron.job('0 0 */6 * * *', () =>
//   runGitFetcher().catch(err => console.error(err))
// );

const GetCovidJob = cron.job('0 0 */6 * * *', () =>
  runGetCovid().catch(err => console.error(err))
);

const timeSeriesJob = cron.job('0 30 */12 * * *', () =>
  runTimeSeriesFetcher().catch(err => console.error(err))
);

// runGitFetcher().catch(err => console.error(err))
runGetCovid().catch(err => console.error(err))
//runTimeSeriesFetcher().catch(err => console.error(err))
runNewsWriter().catch(err => console.error(err))


newsJob.start();
GetCovidJob.start();
timeSeriesJob.start();

//virusDataJob.start();

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
